//
//  JYCircleImageView.m
//  图片轮播器
//
//  Created by 商俊怡 on 15/8/11.
//  Copyright (c) 2015年 商俊怡. All rights reserved.
//

#import "JYCircleImageView.h"

#define imageW [UIScreen mainScreen].bounds.size.width

@interface JYCircleImageView () <UIScrollViewDelegate>
@property(nonatomic, strong) UIScrollView *scrollView;
@property(nonatomic, strong) UIPageControl *pageControl;

@property(nonatomic,assign) NSInteger imageCount;
@end
@implementation JYCircleImageView

- (instancetype)initWithFrame:(CGRect)frame andImageCount:(NSInteger)imageCount
{
    self = [super initWithFrame:frame];
    if (self) {
        self.imageCount = imageCount;
        [self setupCircleImage];
    }
    return self;
}

// 设置轮播器
-(void)setupCircleImage {
    // 初始化scrollView，并添加到控制器
    self.scrollView = [[UIScrollView alloc] init];
    self.scrollView.frame = CGRectMake(0, 0, imageW, 100);

    // 设置scrollView属性
    self.scrollView.contentSize = CGSizeMake(imageW * (self.imageCount+2), 100);
    self.scrollView.bounces = YES;
    self.scrollView.pagingEnabled = YES;
    self.scrollView.showsHorizontalScrollIndicator = NO;

    // 设置初始位置
    [self.scrollView setContentOffset:CGPointMake(imageW, 0)];

    [self addSubview:self.scrollView];
    
    // 设置代理
    self.scrollView.delegate = self;
    
    
    // 设置pageControl
    self.pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(imageW - 100, self.frame.size.height - 14, 100, 10)];
    self.pageControl.numberOfPages = self.imageCount;

    [self addSubview:self.pageControl];
    
    // 设置图片
    [self setupImage];
    
    // 设置定时器
    self.timer = [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(nextImage) userInfo:nil repeats:YES];
    
}


// 设置图片
-(void)setupImage {
    NSLog(@"%zd",self.imageCount);
    for (int i = 0; i < (self.imageCount+2); i ++) {
        UIImage *img = [[UIImage alloc] init];
        if (i == 0) {
            img = [UIImage imageNamed:[NSString stringWithFormat:@"img_%02d",5]];
        }else if (i == 6){
            img = [UIImage imageNamed:[NSString stringWithFormat:@"img_%02d",1]];
        }else{
            img = [UIImage imageNamed:[NSString stringWithFormat:@"img_%02d",i]];
        }
        UIButton *imgBtn = [[UIButton alloc] init];
        [imgBtn setBackgroundImage:img forState:UIControlStateNormal];
        imgBtn.adjustsImageWhenHighlighted = NO;
        NSInteger imgX = imageW * i ;
        NSInteger imgY = 0;
        imgBtn.frame = CGRectMake(imgX, imgY, imageW, self.frame.size.height);
        [imgBtn addTarget:self action:@selector(imgTouch) forControlEvents:UIControlEventTouchUpInside];
        [self.scrollView addSubview:imgBtn];
    }
}

// 图片点击事件
-(void) imgTouch{
    NSLog(@"快来买！");
}

// 下一页
-(void) nextImage{
    NSInteger currentPage = self.pageControl.currentPage;
    currentPage++;
    currentPage = currentPage > self.pageControl.numberOfPages ? 0 :currentPage;
    [self.scrollView setContentOffset:CGPointMake(imageW * (currentPage+1), 0)animated:YES];
}

#pragma MARK - UIScrollView代理方法

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    // 设置页码
    self.pageControl.currentPage = (scrollView.contentOffset.x + imageW / 2) / imageW-1;
}

-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    // 当用开始户拖拽时，移除定时器
    [self.timer invalidate];
    self.timer = nil;
}
-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    // 当用户停止拖拽时，重新开启定时器
    self.timer = [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(nextImage) userInfo:nil repeats:YES];
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    // 当手动拖拽，偷换
    if (scrollView.contentOffset.x == 0) {
        [self.scrollView setContentOffset:CGPointMake(imageW*self.imageCount, 0)];
    }else if(scrollView.contentOffset.x == imageW*(self.imageCount+1)){
        [self.scrollView setContentOffset:CGPointMake(imageW, 0)];
    }
}
-(void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView{
    // 当非手指拖拽，偷换
    if(scrollView.contentOffset.x == imageW*(self.imageCount+1)){
        [self.scrollView setContentOffset:CGPointMake(imageW, 0)];
    }
}


@end
