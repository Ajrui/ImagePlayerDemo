//
//  ViewController.m
//  图片轮播器
//
//  Created by 商俊怡 on 15/7/30.
//  Copyright (c) 2015年 商俊怡. All rights reserved.
//

#import "ViewController.h"
#import "JYCircleImageView.h"
@interface ViewController ()
@property(nonatomic, strong) JYCircleImageView *circleImageView;
@end 

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.circleImageView = [[JYCircleImageView alloc] initWithFrame:CGRectMake(0, 64, self.view.bounds.size.width, 100) andImageCount:5];

    [self.view addSubview:self.circleImageView];
}

@end
