//
//  JYSurpriseViewController.m
//  图片轮播器
//
//  Created by 商俊怡 on 15/8/4.
//  Copyright (c) 2015年 商俊怡. All rights reserved.
//

#import "JYSurpriseViewController.h"
#import "JYSurpriseCollectionViewCell.h"

#define surpriseY (self.view.bounds.size.height - 269)

@interface JYSurpriseViewController () <UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate>
@property(nonatomic, strong) UICollectionView *surpriseCollectionView;

@property(nonatomic, strong) UIScrollView *scrollView;
@property(nonatomic, strong) UIPageControl *pageControl;
@property(nonatomic, strong) NSTimer *timer;

@end
static int imageCount = 5;
@implementation JYSurpriseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupCircleImage];
    [self setupSurpriseView];
}

// 设置更多惊喜
-(void)setupSurpriseView{
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    
    int collectionW = self.view.bounds.size.width;
    self.surpriseCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, surpriseY, collectionW, collectionW/3) collectionViewLayout:flowLayout];
    [self.surpriseCollectionView setBackgroundColor:[UIColor whiteColor]];
    self.surpriseCollectionView.pagingEnabled = YES;
    self.surpriseCollectionView.showsHorizontalScrollIndicator = NO;
    
    [self.surpriseCollectionView registerClass:[JYSurpriseCollectionViewCell class] forCellWithReuseIdentifier:@"cell"];
    [self.view addSubview:self.surpriseCollectionView];
    
    // 设置代理和数据源
    self.surpriseCollectionView.delegate = self;
    self.surpriseCollectionView.dataSource = self;
}

-(void)setupCircleImage {
    // 初始化scrollView，并添加到控制器
    self.scrollView = [[UIScrollView alloc] init];
    self.scrollView.frame = CGRectMake(0, 64, self.view.bounds.size.width, 100);
    //    self.scrollView.backgroundColor = [UIColor redColor];
    self.scrollView.tag = 1;
    [self.view addSubview:self.scrollView];
    
    // 设置代理
    self.scrollView.delegate = self;
    
    // 设置scrollView属性
    self.scrollView.contentSize = CGSizeMake(self.view.bounds.size.width * (imageCount+2), 200);
    self.scrollView.bounces = YES;
    self.scrollView.pagingEnabled = YES;
    self.scrollView.showsHorizontalScrollIndicator = NO;
    
    // 设置pageControl
    self.pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(self.view.frame.size.width - 100, 149, 100, 10)];
    self.pageControl.numberOfPages = imageCount;
    [self.view addSubview:self.pageControl];
    
    // 设置图片
    [self setupImage];
    
    // 设置定时器
    self.timer = [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(nextImage) userInfo:nil repeats:YES];
    
    // 设置初始位置
    [self.scrollView setContentOffset:CGPointMake(self.view.bounds.size.width, 0)];
}

// 设置图片
-(void)setupImage {
    CGFloat imgW = self.view.bounds.size.width;
    CGFloat imgH = self.scrollView.frame.size.height;
    for (int i = 0; i < imageCount+2; i ++) {
        UIImage *img = [[UIImage alloc] init];
        if (i == 0) {
            img = [UIImage imageNamed:[NSString stringWithFormat:@"img_%02d",5]];
        }else if (i == 6){
            img = [UIImage imageNamed:[NSString stringWithFormat:@"img_%02d",1]];
        }else{
            img = [UIImage imageNamed:[NSString stringWithFormat:@"img_%02d",i]];
        }
        UIButton *imgBtn = [[UIButton alloc] init];
        [imgBtn setBackgroundImage:img forState:UIControlStateNormal];
        imgBtn.adjustsImageWhenHighlighted = NO;
        NSInteger imgX = self.view.bounds.size.width * i ;
        NSInteger imgY = 0;
        imgBtn.frame = CGRectMake(imgX, imgY, imgW, imgH);
        [imgBtn addTarget:self action:@selector(imgTouch) forControlEvents:UIControlEventTouchUpInside];
        [self.scrollView addSubview:imgBtn];
    }
}

// 图片点击事件
-(void) imgTouch{
    NSLog(@"快来买！");
}

// 下一页
-(void) nextImage{
    NSInteger currentPage = self.pageControl.currentPage;
    currentPage++;
    currentPage = currentPage > self.pageControl.numberOfPages ? 0 :currentPage;
    [self.scrollView setContentOffset:CGPointMake(self.view.bounds.size.width * (currentPage+1), 0)animated:YES];
}

#pragma MARK - UIScrollView代理方法

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    // 设置页码
    if (scrollView.tag == 1) {
        self.pageControl.currentPage = (scrollView.contentOffset.x + self.view.frame.size.width / 2) / self.scrollView.frame.size.width-1;
    }
}

-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    // 当用开始户拖拽时，移除定时器
    if (scrollView.tag == 1) {
        [self.timer invalidate];
        self.timer = nil;
    }
}
-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    // 当用户停止拖拽时，重新开启定时器
    if (scrollView.tag == 1) {
        self.timer = [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(nextImage) userInfo:nil repeats:YES];
    }
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    // 当手动拖拽，偷换
    if (scrollView.tag == 1) {
        if (scrollView.contentOffset.x == 0) {
            [self.scrollView setContentOffset:CGPointMake(self.view.frame.size.width*imageCount, 0)];
        }else if(scrollView.contentOffset.x == self.view.frame.size.width*(imageCount+1)){
            [self.scrollView setContentOffset:CGPointMake(self.view.frame.size.width, 0)];
        }
    }
}
-(void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView{
    // 当非手指拖拽，偷换
    if (scrollView.tag == 1) {
        if(scrollView.contentOffset.x == self.view.frame.size.width*(imageCount+1)){
            [self.scrollView setContentOffset:CGPointMake(self.view.frame.size.width, 0)];
        }
    }
}

#pragma mark - collectionView 的数据源方法
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 15;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *ID = @"cell";
    JYSurpriseCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:ID forIndexPath:indexPath];
    [cell sizeToFit];
    if (!cell) {
        // 如果没有 cell 可以复用，则需要实例化新的 cell
//        JYSurpriseCollectionViewCell *cell = [[JYSurpriseCollectionViewCell alloc] initWithFrame:CGRectMake(0, 0, 100, 200)];
//        cell.restorationIdentifier = @"cell";
        NSLog(@"没有 cell 可以复用，需要实例化");
    }
    [cell.imgView setImage:[UIImage imageNamed:@"7.jpg"]];
    cell.textLabel.text = [NSString stringWithFormat:@"   ￥%zd ",indexPath.row];
    return cell;
}

#pragma mark - UICollectionViewDelegateFlowLayout
-(CGSize) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake((collectionView.bounds.size.width )/3, (collectionView.bounds.size.width)/3);
}

//-(CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
//    return 1;
//}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 0;
}

#pragma mark - collectionView 的代理方法
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"%zd",indexPath.item);
}
@end
