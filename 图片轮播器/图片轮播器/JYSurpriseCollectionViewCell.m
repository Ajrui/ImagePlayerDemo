//
//  JYSurpriseCollectionViewCell.m
//  图片轮播器
//
//  Created by 商俊怡 on 15/8/4.
//  Copyright (c) 2015年 商俊怡. All rights reserved.
//

#import "JYSurpriseCollectionViewCell.h"

@implementation JYSurpriseCollectionViewCell
-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        // 设置 cell 的背景色
        self.backgroundColor = [UIColor whiteColor];
        
        // 设置图片
        self.imgView = [[UIImageView alloc] initWithFrame:CGRectMake(5, 5, CGRectGetWidth(self.frame)-10, CGRectGetWidth(self.frame)-10)];
        [self.imgView setBackgroundColor:[UIColor groupTableViewBackgroundColor]];
        [self addSubview:self.imgView];
        // 设置文字
        self.textLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, CGRectGetMaxY(self.imgView.frame) - 22, self.imgView.frame.size.width, 22)];
        self.textLabel.backgroundColor = [UIColor blackColor];
        self.textLabel.numberOfLines = 0;
        self.textLabel.font = [UIFont systemFontOfSize:11.0];
        self.textLabel.textColor = [UIColor whiteColor];
        self.textLabel.textAlignment = NSTextAlignmentLeft;
        self.textLabel.alpha = 0.5;
        [self addSubview:self.textLabel];
        
        // 设置分割线
//        UIView *leftLine = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1, self.bounds.size.height)];
//        leftLine.backgroundColor = [UIColor grayColor];
//        [self addSubview:leftLine];
        UIView *rightLine = [[UIView alloc] initWithFrame:CGRectMake(self.bounds.size.width - 1, 15, 1, self.bounds.size.height-30)];
        rightLine.backgroundColor = [UIColor grayColor];
        [self addSubview:rightLine];
    }
    return self;
}
@end
