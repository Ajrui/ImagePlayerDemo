//
//  JYScrollView.m
//  图片轮播器
//
//  Created by 商俊怡 on 15/8/11.
//  Copyright (c) 2015年 商俊怡. All rights reserved.
//

#import "JYScrollView.h"

#define imageW [UIScreen mainScreen].bounds.size.width

@interface JYScrollView () <UIScrollViewDelegate>
@property(nonatomic, strong) UIPageControl *pageControl;
@property(nonatomic, strong) NSTimer *timer;
@property(nonatomic,assign) NSInteger imageCount;
@end

@implementation JYScrollView

-(instancetype)initWithFrame:(CGRect)frame andCount:(NSInteger)count{
    self = [super initWithFrame:frame];
    
    self.imageCount = count;
    // 设置scrollView属性
    self.contentSize = CGSizeMake([UIScreen mainScreen].bounds.size.width * (self.imageCount+2), 200);
    self.bounces = YES;
    self.pagingEnabled = YES;
    self.showsHorizontalScrollIndicator = NO;
    
    // 设置代理
    self.delegate = self;
    
    // 设置pageControl
    self.pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(imageW - 100, 100, 100, 10)];
    self.pageControl.numberOfPages = count;
    [self addSubview:self.pageControl];
    NSLog(@"%@",self.pageControl);
    
    // 设置图片
    [self setupImage];
    
    // 设置定时器
    self.timer = [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(nextImage) userInfo:nil repeats:YES];
    
    // 设置初始位置
    [self setContentOffset:CGPointMake([UIScreen mainScreen].bounds.size.width, 0)];
    
    return self;
}

// 设置图片
-(void)setupImage {
    for (int i = 0; i < (self.imageCount+2); i ++) {
        UIImage *img = [[UIImage alloc] init];
        if (i == 0) {
            img = [UIImage imageNamed:[NSString stringWithFormat:@"img_%02d",5]];
        }else if (i == 6){
            img = [UIImage imageNamed:[NSString stringWithFormat:@"img_%02d",1]];
        }else{
            img = [UIImage imageNamed:[NSString stringWithFormat:@"img_%02d",i]];
        }
        UIButton *imgBtn = [[UIButton alloc] init];
        [imgBtn setBackgroundImage:img forState:UIControlStateNormal];
        imgBtn.adjustsImageWhenHighlighted = NO;
        NSInteger imgX = imageW * i ;
        NSInteger imgY = 0;
        imgBtn.frame = CGRectMake(imgX, imgY, imageW, self.frame.size.height);
        [imgBtn addTarget:self action:@selector(imgTouch) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:imgBtn];
    }
}

// 图片点击事件
-(void) imgTouch{
    NSLog(@"快来买！");
}

// 下一页
-(void) nextImage{
    NSInteger currentPage = self.pageControl.currentPage;
    currentPage++;
    currentPage = currentPage > self.pageControl.numberOfPages ? 0 :currentPage;
    [self setContentOffset:CGPointMake(imageW * (currentPage+1), 0)animated:YES];
}

#pragma MARK - UIScrollView代理方法

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    // 设置页码
    self.pageControl.currentPage = (scrollView.contentOffset.x + imageW / 2) / imageW-1;
}

-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    // 当用开始户拖拽时，移除定时器
    [self.timer invalidate];
    self.timer = nil;
}
-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    // 当用户停止拖拽时，重新开启定时器
    self.timer = [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(nextImage) userInfo:nil repeats:YES];
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    // 当手动拖拽，偷换
    if (scrollView.contentOffset.x == 0) {
        [self setContentOffset:CGPointMake(imageW*self.imageCount, 0)];
    }else if(scrollView.contentOffset.x == imageW*(self.imageCount+1)){
        [self setContentOffset:CGPointMake(imageW, 0)];
    }
}
-(void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView{
    // 当非手指拖拽，偷换
    if(scrollView.contentOffset.x == imageW*(self.imageCount+1)){
        [self setContentOffset:CGPointMake(imageW, 0)];
    }
}


@end
