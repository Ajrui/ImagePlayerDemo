//
//  JYCircleImageView.h
//  图片轮播器
//
//  Created by 商俊怡 on 15/8/11.
//  Copyright (c) 2015年 商俊怡. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JYCircleImageView : UIView
/**
 *  定时器
 */
@property(nonatomic, strong) NSTimer *timer;

// 初始化方法
- (instancetype)initWithFrame:(CGRect)frame andImageCount:(NSInteger)imageCount;
// 下一页
-(void) nextImage;
@end
