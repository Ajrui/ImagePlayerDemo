//
//  JYScrollView.h
//  图片轮播器
//
//  Created by 商俊怡 on 15/8/11.
//  Copyright (c) 2015年 商俊怡. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JYScrollView : UIScrollView
-(instancetype)initWithFrame:(CGRect)frame andCount:(NSInteger)count;
@end
