//
//  JYViewController.m
//  图片轮播器
//
//  Created by 商俊怡 on 15/7/31.
//  Copyright (c) 2015年 商俊怡. All rights reserved.
//

#import "JYViewController.h"

@interface JYViewController () <UIScrollViewDelegate>
@property(nonatomic, strong) UIScrollView *scrollView;
@property(nonatomic, strong) UIPageControl *pageControl;
@property(nonatomic, strong) NSTimer *timer;

@end

@implementation JYViewController

static int imageCount = 5;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // 无限轮播
    [self setupCircleImage];
    
}
// 设置轮播器
-(void)setupCircleImage {
    // 初始化scrollView，并添加到控制器
    self.scrollView = [[UIScrollView alloc] init];
    self.scrollView.frame = CGRectMake(0, 20, self.view.bounds.size.width, 200);
    //    self.scrollView.backgroundColor = [UIColor redColor];
    [self.view addSubview:self.scrollView];
    
    // 设置代理
    self.scrollView.delegate = self;
    
    // 设置scrollView属性
    self.scrollView.contentSize = CGSizeMake(self.view.bounds.size.width * (imageCount+2), 200);
    self.scrollView.bounces = YES;
    self.scrollView.pagingEnabled = YES;
    self.scrollView.showsHorizontalScrollIndicator = NO;
    
    // 设置pageControl
    self.pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(self.view.frame.size.width - 100, 157, 100, 10)];
    self.pageControl.numberOfPages = imageCount;
    [self.view addSubview:self.pageControl];
    
    // 设置图片
    [self setupImage];
    
    // 设置定时器
    self.timer = [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(nextImage) userInfo:nil repeats:YES];
    
    // 设置初始位置
    [self.scrollView setContentOffset:CGPointMake(self.view.bounds.size.width, 0)];
}

// 设置图片
-(void)setupImage {
    CGFloat imgW = self.view.bounds.size.width;
    CGFloat imgH = self.scrollView.frame.size.height;
    for (int i = 0; i < imageCount+2; i ++) {
        UIImage *img = [[UIImage alloc] init];
        if (i == 0) {
            img = [UIImage imageNamed:[NSString stringWithFormat:@"img_%02d",5]];
        }else if (i == 6){
            img = [UIImage imageNamed:[NSString stringWithFormat:@"img_%02d",1]];
        }else{
            img = [UIImage imageNamed:[NSString stringWithFormat:@"img_%02d",i]];
        }
        UIButton *imgBtn = [[UIButton alloc] init];
        [imgBtn setBackgroundImage:img forState:UIControlStateNormal];
        imgBtn.adjustsImageWhenHighlighted = NO;
        NSInteger imgX = self.view.bounds.size.width * i ;
        NSInteger imgY = 0;
        imgBtn.frame = CGRectMake(imgX, imgY, imgW, imgH);
        [imgBtn addTarget:self action:@selector(imgTouch) forControlEvents:UIControlEventTouchUpInside];
        [self.scrollView addSubview:imgBtn];
    }
}

// 图片点击事件
-(void) imgTouch{
    NSLog(@"快来买！");
}

// 下一页
-(void) nextImage{
    NSInteger currentPage = self.pageControl.currentPage;
    currentPage++;
    currentPage = currentPage > self.pageControl.numberOfPages ? 0 :currentPage;
    [self.scrollView setContentOffset:CGPointMake(self.view.bounds.size.width * (currentPage+1), 0)animated:YES];
}

#pragma MARK - UIScrollView代理方法

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    // 设置页码
    self.pageControl.currentPage = (scrollView.contentOffset.x + self.view.frame.size.width / 2) / self.scrollView.frame.size.width-1;
}

-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    // 当用开始户拖拽时，移除定时器
    [self.timer invalidate];
    self.timer = nil;
}
-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    // 当用户停止拖拽时，重新开启定时器
    self.timer = [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(nextImage) userInfo:nil repeats:YES];
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    // 当手动拖拽，偷换
    if (scrollView.contentOffset.x == 0) {
        [self.scrollView setContentOffset:CGPointMake(self.view.frame.size.width*imageCount, 0)];
    }else if(scrollView.contentOffset.x == self.view.frame.size.width*(imageCount+1)){
        [self.scrollView setContentOffset:CGPointMake(self.view.frame.size.width, 0)];
    }
}
-(void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView{
    // 当非手指拖拽，偷换
    if(scrollView.contentOffset.x == self.view.frame.size.width*(imageCount+1)){
        [self.scrollView setContentOffset:CGPointMake(self.view.frame.size.width, 0)];
    }
}
@end
